FROM debian:stretch as build

RUN apt update && apt install -y gnupg2 dpkg-dev curl apt-utils && \
    curl -L -o /tmp/nginx_signing.key http://nginx.org/keys/nginx_signing.key && \
    apt-key add /tmp/nginx_signing.key && \
    echo "deb http://nginx.org/packages/mainline/debian/ stretch nginx" >> /etc/apt/sources.list && \
    echo "deb-src http://nginx.org/packages/mainline/debian/ stretch nginx" >> /etc/apt/sources.list

# lua-nginx-module only supports nginx <=1.11.2
ENV NGINX_VERSION 1.19.6
ENV NGINX_FULL_VERSION ${NGINX_VERSION}-1~stretch
ENV ECHO_VERSION 0.62
ENV LUA_VERSION 0.10.19

RUN mkdir -p /usr/src/nginx
WORKDIR /usr/src/nginx

# Download nginx source
RUN apt-get update && \
    apt-get install -y libluajit-5.1-dev && \
    apt-get source nginx=${NGINX_FULL_VERSION} && \
    apt-get build-dep -y nginx=${NGINX_FULL_VERSION} && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/nginx/nginx-${NGINX_VERSION}/debian/modules/

# Download ECHO module
RUN curl -L https://github.com/openresty/echo-nginx-module/archive/v${ECHO_VERSION}.tar.gz | tar xz && \
    ln -s echo-nginx-module-${ECHO_VERSION} nginx-echo-module

# Download LUA module
RUN curl -L https://github.com/openresty/lua-nginx-module/archive/v${LUA_VERSION}.tar.gz | tar xz && \
    ln -s lua-nginx-module-${LUA_VERSION} lua-nginx-module

# Add modules to build nginx debian rules
ENV ECHO_MODULE_SOURCE "\\\/usr\\\/src\\\/nginx\\\/nginx-${NGINX_VERSION}\\\/debian\\\/modules\\\/nginx-echo-module"
ENV LUA_MODULE_SOURCE "\\\/usr\\\/src\\\/nginx\\\/nginx-${NGINX_VERSION}\\\/debian\\\/modules\\\/lua-nginx-module"
RUN sed -i "s#--with-ipv6#--with-ipv6 --add-module=${ECHO_MODULE_SOURCE} --add-module=${LUA_MODULE_SOURCE}#g" /usr/src/nginx/nginx-${NGINX_VERSION}/debian/rules

# Build nginx debian package
WORKDIR /usr/src/nginx/nginx-${NGINX_VERSION}
RUN dpkg-buildpackage -b
#RUN echo $PWD && ls -l ../ 

FROM debian:9
# Install nginx
ENV NGINX_VERSION 1.19.6
ENV NGINX_FULL_VERSION ${NGINX_VERSION}-1~stretch
WORKDIR /usr/src/nginx
COPY --from=build /usr/src/nginx//nginx_${NGINX_FULL_VERSION}_amd64.deb .

RUN apt update && apt install libssl1.1 -y && dpkg -i nginx_${NGINX_FULL_VERSION}_amd64.deb

# Make snakeoil certificates available
RUN apt-get update && \
    apt-get install -qy ssl-cert && \
    apt-get autoremove -yqq && \
    apt-get clean -yqq && \
    rm -rf /usr/src/nginx

# Forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
COPY nginx.conf /etc/nginx/nginx.conf 

WORKDIR /etc/nginx

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]

